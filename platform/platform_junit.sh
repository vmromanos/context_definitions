#!/bin/bash

set -e

export ANT_OPTS="$ANT_OPTS_JUNIT"

echo "ANT_OPTS = $ANT_OPTS"
echo "ANT_OPTS_JUNIT = $ANT_OPTS_JUNIT"

execute_test (){
  TEST=$1
  echo "Execute test $TEST"
  set +e
  ant run.test -Dtest=$TEST > junit-$TEST.log 2>&1
  RESULT=$?
  set -e

  ALL_TESTS_PASS=$(cat src/result-$TEST.xml | grep "^<testsuite" | grep "errors=\"0\"" | grep "failures=\"0\"" || true)
  if [ "$RESULT" != "0" ] || [ "$ALL_TESTS_PASS" = "" ] ; then
     echo -e "\n\n\n * FAILURE:  ant run.test -Dtest=$TEST" | tee -a errors.log
     echo -e "RESULTS: ${BUILD_URL}testReport/" | tee -a errors.log
     echo -e "LOG: ${BUILD_URL}artifact/junit-$TEST.log/*view*/ " | tee -a errors.log
  fi
}

TEST_SUITES="org.openbravo.service.external.integration.test.ExternalIntegrationTestSuite
org.openbravo.service.integration.sapecc.SapEccTestSuite
org.openbravo.externaldata.integration.examples.test.EDLTestSuite"

for TEST in $TEST_SUITES
do
  execute_test $TEST
done
