#!/usr/bin/python3

import json
from datetime import date
from os import path

base_json = "erp/erp.json"
files_to_merge = [
    "platform/platform.json",
    "retail/retail.json",
    "functional/functional-basic.json",
    "functional/functional-retail.json",
    "retail/retail-pack.json"
]

mods_by_javapackage = {}
deps_by_javapackage = {}

# first pass over json: find & merge all mods (removing duplicates)
for to_merge in files_to_merge:
    print("1st pass over file %s" % to_merge)
    j2 = json.load(open(path.join('..', to_merge)))
    for mod in j2["mods"]:
        javapackage = mod["javapackage"]
        if javapackage not in mods_by_javapackage:
            mods_by_javapackage[javapackage] = mod

# second pass over json: deps entries, only add if not present in mod or dep
for to_merge in files_to_merge:
    print("2nd pass over file %s" % to_merge)
    j2 = json.load(open(path.join('..', to_merge)))
    # some file (i.e. retail-pack.json) don't have deps section
    if "deps" in j2:
        for dep in j2["deps"]:
            javapackage = dep["javapackage"]
            if javapackage not in mods_by_javapackage and javapackage not in deps_by_javapackage:
                deps_by_javapackage[javapackage] = dep

# Build output

# contains erp line (tip of pi)
j = json.load(open(path.join('..', base_json)))

j["desc"] = "Merged config of %s last update %s" % (files_to_merge, date.today())

j["mods"] = []
j["deps"] = []

# handle team.retail special case (they have clone of normal sampledata
# Remove normal sampledata module as having both will fail
del mods_by_javapackage["org.openbravo.retail.sampledata"]

# Handle special cases of conflicting modules

# 2 modules cannot be co-installed (sap connector + magento mappings)
# As pragmatic workaround: remove magento.mappings from mods (to be tested) and move it instead
#   to deps. It is required to be installed as retail test sampledata depends on it
deps_by_javapackage[
    "org.openbravo.service.integration.magento.mappings"
] = mods_by_javapackage["org.openbravo.service.integration.magento.mappings"]
del mods_by_javapackage["org.openbravo.service.integration.magento.mappings"]

# org.openbravo.retail.api conflicts with sap connector module in same way
deps_by_javapackage[
    "org.openbravo.retail.api"
] = mods_by_javapackage["org.openbravo.retail.api"]
del mods_by_javapackage["org.openbravo.retail.api"]


# sorted output for mods
for javapackage in sorted(mods_by_javapackage.keys()):
    mod = mods_by_javapackage[javapackage]
    j["mods"].append(mod)

# sorted output for deps
for javapackage in sorted(deps_by_javapackage.keys()):
    dep = deps_by_javapackage[javapackage]
    j["deps"].append(dep)

print("Count mods: %d" % len(j["mods"]))
print("Count deps: %d" % len(j["deps"]))

json.dump(j, open("merged.json", "w"), indent="\t")
